<?php

// get all breeds
$app -> get('/breeds', function ($request, $response, $args) {
    $sth = $this -> db -> prepare("SELECT * FROM breeds ORDER BY name");
    $sth -> execute();
    $breeds = $sth -> fetchAll();
    return $this -> response -> withJson($breeds);
});

// get breed by id
$app -> get('/breeds/[{id}]', function ($request, $response, $args) {
    $sth = $this -> db -> prepare("SELECT * FROM breeds WHERE id=:id");
    $sth -> bindParam("id", $args['id']);
    $sth -> execute();
    $breeds = $sth -> fetchObject();
    return $this -> response -> withJson($breeds);
});

// add new breed
$app -> post('/breeds', function ($request, $response) {
    $input = $request -> getParsedBody();
    $sql = "INSERT INTO breeds (name, country, size, energy, hypoallergenic) VALUES (:name, :country, :size, :energy, :hypoallergenic)";
    $sth = $this -> db -> prepare($sql);
    $sth -> execute(['name' => $input['name'], 'country' => $input['country'], 'size' => $input['size'], 'energy' => $input['energy'], 'hypoallergenic' => $input['hypoallergenic']]);
    $input['id'] = $this -> db -> lastInsertId();
    return $this -> response -> withJson($input);
});

// delete breed by id
$app -> delete('/breeds/[{id}]', function ($request, $response, $args) {
    $sth = $this -> db -> prepare("DELETE FROM breeds WHERE id=:id");
    $sth -> bindParam("id", $args['id']);
    $sth -> execute();
    $breeds = $sth -> fetchAll();
    return $this -> response -> withJson($breeds);
});

// update breed with given id
$app -> put('/breeds/[{id}]', function ($request, $response, $args) {
    $input = $request -> getParsedBody();
    $sql = "UPDATE breeds SET name=:name, country=:country, size=:size, energy=:energy, hypoallergenic=:hypoallergenic WHERE id=:id";
    $sth = $this -> db -> prepare($sql);
    $sth -> bindParam("id", $args['id']);
    $sth -> execute(['name' => $input['name'], 'country' => $input['country'], 'size' => $input['size'], 'energy' => $input['energy'], 'hypoallergenic' => $input['hypoallergenic']]);
    $input['id'] = $args['id'];
    return $this -> response -> withJson($input);
});

// get breed by param
$app -> get('/breeds/search/[{param}/[{query}]]', function ($request, $response, $args) {
	$sth = $this -> db -> prepare("SELECT * FROM breeds WHERE ".$args['param']." LIKE :query ORDER BY name");
    $query = "%".$args['query']."%";
    $sth -> execute(['query' => $query]);
    $breeds = $sth -> fetchAll();
    return $this -> response -> withJson($breeds);
});
